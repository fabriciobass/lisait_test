import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { Heroes } from '../model/heroes.model';
import { HeroesService } from '../services/heroes.service';
import { NetworkService, ConnectionStatus } from '../services/network.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  modelHeroes: any[];
  Herores = {} as Heroes;
  isLoading = false;

  constructor(
    public platform: Platform,
    private networkService: NetworkService,
    public loadingController: LoadingController,
    private router: Router,
    public toastController: ToastController,
    private heroesService: HeroesService
  ) {}

  ngOnInit() {
    //
  }

  ionViewWillEnter(){
    this.getAllHeroes();
  }

  async sendOffline() {
    this.platform.ready().then(async () => {
      if (this.networkService.getCurrentNetworkStatus() === ConnectionStatus.Offline) {
        await this.presentToast('No internet connection, check!');
      } else {
        this.loadingPresent('Sending data OFFLine ...');
        await this.heroesService.getAllHeroesOffline()
        .then(async (result: any[]) => {
          if (result.length > 0) {
            for (let i = 0; i < result.length; i++) {
              var heroes: any;
              heroes = '{ "Name": "' + result[i].Name + '", "CategoryId": "' + result[i].CategoryId+ '", "Active": "' + result[i].Active + '" }';
              heroes = JSON.parse(heroes);    
              await this.heroesService.insertHeroes(heroes)
              .then(async (response) => {
                if(response.status === 201) {          
                  await this.presentToast('Hero successfully inserted!');
                  await this.heroesService.deleteHeroesOffline(result[i].Id);
                } else {
                  await this.presentToast('Error when registering!');
                }
              }, (error) => {
                console.log(error);
              });
            }
            await this.presentToast('Records sent successfully!');
            this.loadingDismiss();
            await this.getAllHeroes();
          } else {
            await this.presentToast('There are no records for submission!');
            this.loadingDismiss();
          }
        }, (error) => {
          console.log(error);
        });
      }
    });    
  }

  async getAllHeroes(){
    this.platform.ready().then(async () => {
      this.loadingPresent('Please wait ...');
        try {
          await this.heroesService.getAllHeroes()
          .then((response) => {
            if(response.status === 200) {
              if(response.data) {
                response.data = JSON.parse(response.data);      
                this.modelHeroes = response.data;
              } else {
                this.presentToast('Invalid data!');
              }
            } else {
              this.presentToast('Error returning data!');
            }
            this.loadingDismiss();
          }, (error) => {
            this.presentToast(error);
            this.loadingDismiss();
          });  
        } catch (error) {
          this.loadingDismiss();
          this.presentToast(error); 
        }
    });    
  }

  pageAdd() {
    this.router.navigate(['/heros-add']);
  }

  async loadingPresent(msg: string) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: msg,
      spinner: 'circles' 
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort laoding'));
        }
      });
    });
  }

  async loadingDismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('loading dismissed'));
  }

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
