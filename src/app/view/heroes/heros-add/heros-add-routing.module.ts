import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HerosAddPage } from './heros-add.page';

const routes: Routes = [
  {
    path: '',
    component: HerosAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HerosAddPageRoutingModule {}
