import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HerosAddPageRoutingModule } from './heros-add-routing.module';

import { HerosAddPage } from './heros-add.page';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    HerosAddPageRoutingModule
  ],
  declarations: [HerosAddPage]
})
export class HerosAddPageModule {}
